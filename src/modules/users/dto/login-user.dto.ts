import { IsString, IsEmail, IsNotEmpty } from 'class-validator';

export class LoginUserDto {
    @IsEmail()
    @IsNotEmpty({ message: 'Email is required.' })
    readonly id: string;

    @IsString()
    @IsNotEmpty({ message: 'Password is required.' })
    readonly password: string;
}
