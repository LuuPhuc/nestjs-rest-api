import { IsString, MinLength, MaxLength, Matches, IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
    @IsString()
    @MaxLength(255)
    firstName: string;

    @IsString()
    @MaxLength(255)
    lastName: string;

    @IsEmail()
    @IsNotEmpty({ message: 'Email is required.' })
    readonly email: string;

    @IsString()
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z)(?=.*[a-z]).*$/, {
        message: 'Password to weak',
    })
    @IsNotEmpty({ message: 'Password is required.' })
    readonly password: string;
}
