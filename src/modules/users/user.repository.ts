import { EntityRepository, Repository } from 'typeorm';
import { UserEntity } from './user.entity';

// Library
import * as _ from 'lodash';

// Constants
import { USER_STATUS } from './constants';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {
    /**
     * Get user by email or username
     * @param {string} id - Email or username
     */
    async getByEmailOrUsername(id: string): Promise<any> {
        const user = await this.createQueryBuilder('users')
            .where('users.email = :id OR users.username = :id', { id })
            .andWhere('users.status = :status', { status: `${USER_STATUS.ACTIVE}` })
            .getOne();
        return user;
    }
}
