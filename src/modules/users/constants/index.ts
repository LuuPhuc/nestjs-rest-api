export const USER_STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive',
};
export const USER_TYPES = {
    ADMIN: 'admin',
    USER: 'user',
};
