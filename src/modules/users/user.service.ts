// Nest
import {
    Injectable,
    UnauthorizedException,
    NotFoundException,
    ConflictException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection } from 'typeorm';

// Library
import * as _ from 'lodash';
import multer from 'multer';

// Utils
import { hashPassword, comparePassword } from '../../utils';

// Dto
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

// Repository
import { UserRepository } from './user.repository';

// Services
import { AuthService } from '../auth/auth.service';

// Library
import { v4 as uuidV4 } from 'uuid';

const userTransform = [
    'id',
    'name',
    'username',
    'email',
    'avatarUrl',
    'type',
    'status',
    'phone',
    'created_at',
];

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserRepository) private userRepo: UserRepository,
        private authService: AuthService,
        private connection: Connection,
    ) {}

    /**
     * Register user
     * @param createUserDto
     * @return newUser
     */
    async register(createUserDto: CreateUserDto): Promise<any> {
        const existedUser = await this.userRepo.findOne({ email: createUserDto.email });

        if (existedUser) throw new ConflictException('Email already existed.');

        const newUser = await this.userRepo
            .create({
                ...createUserDto,
                name: `${createUserDto.firstName} ${createUserDto.lastName}`,
                username: uuidV4().replace(/-/g, ''),
                password: await hashPassword(createUserDto.password),
            })
            .save();

        const transformedUser = _.pick(newUser, userTransform);

        // Generate tokens
        const tokens = await this.authService.generateToken(newUser);

        return { user: transformedUser, tokens };
    }

    /**
     * Login
     * @param loginUserDto
     * @return user
     */
    async login(loginUserDto: LoginUserDto): Promise<any> {
        const user = await this.userRepo.getByEmailOrUsername(loginUserDto.id);

        if (!user) throw new NotFoundException('User does not exists.');

        if (!(await comparePassword(loginUserDto.password, user.password))) {
            throw new UnauthorizedException('Incorrect email or password');
        }

        const transformedUser = _.pick(user, userTransform);

        // Generate tokens
        const tokens = await this.authService.generateToken(user);

        return { user: transformedUser, tokens };
    }
}
