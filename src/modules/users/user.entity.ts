import { Entity, Column } from 'typeorm';
import { AbstractEntity } from '../../common/entities/abstract.entity';
import { UserStatus, UserType } from '../../interfaces/index';

@Entity({
    name: 'users',
})
export class UserEntity extends AbstractEntity<UserEntity> {
    @Column({ length: 255, nullable: false, unique: true })
    email: string;

    @Column({ length: 255, nullable: false, unique: false })
    firstName: string;

    @Column({ length: 255, nullable: false, unique: false })
    lastName: string;

    @Column({ length: 255, nullable: false, unique: false })
    name: string;

    @Column({ length: 255, nullable: false, unique: true })
    username: string;

    @Column({ length: 5, nullable: false, unique: false })
    type: string = UserType.User;

    @Column({ length: 255, nullable: true, unique: false })
    avatarUrl: string;

    @Column({ nullable: false, unique: false })
    status: UserStatus = UserStatus.Active;

    @Column({ length: 255, nullable: false, unique: false })
    password: string;
}
