/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, Post, Get, Param, UseInterceptors, Body } from '@nestjs/common';

import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

import { SerializerInterceptor } from '../../serialization/serializer.interceptor';
import { UploadedFiles } from '@nestjs/common/decorators';
import {
    FileFieldsInterceptor,
    FilesInterceptor,
} from '@nestjs/platform-express/multer/interceptors';

@UseInterceptors(SerializerInterceptor)
@Controller('/users')
export class UserController {
    constructor(private userService: UserService) {}

    /**
     * Register controller
     * @param createUserDto
     * @returns newUser
     */
    @Post('/register')
    async register(@Body() createUserDto: CreateUserDto) {
        const newUser = await this.userService.register(createUserDto);
        return newUser;
    }

    // @Post('upload/id-recognition')
    // @UseInterceptors(
    //     FileFieldsInterceptor([
    //         { name: 'front_of', maxCount: 1 },
    //         { name: 'back_side', maxCount: 1 },
    //     ]),
    // )
    // uploadFile(@UploadedFiles() files: Array<Express.Multer.File>) {
    //     console.log('______', files);
    // }

    /**
     * Login controller
     * @param loginUserDto
     * @returns user
     */
    @Post('/login')
    async login(@Body() loginUserDto: LoginUserDto) {
        const user = await this.userService.login(loginUserDto);
        return user;
    }
}
