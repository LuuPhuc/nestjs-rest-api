// Nest
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';

import config from '../../config';

// Library
import ms from 'ms';
import { Duration, DateTime } from 'luxon';
import { v4 as uuidV4 } from 'uuid';

// Entity
import { UserEntity } from '../users/user.entity';

const accessTokenDuration = Duration.fromMillis(ms(config.ACCESS_TOKEN_EXP));
const refreshTokenDuration = Duration.fromMillis(ms(config.REFRESH_TOKEN_EXP));

@Injectable()
export class AuthService {
    constructor(private readonly jwtService: JwtService) {}

    async generateToken(user: UserEntity): Promise<any> {
        const accessToken = await this.jwtService.sign({
            sub: user.id,
            name: user.name,
            email: user.email,
            role: user.type,
            exp: DateTime.utc().plus(accessTokenDuration).toSeconds(),
        });

        const refreshToken = `${user.id}#${uuidV4().replace(/-/g, '')}`;
        return {
            token_type: 'Bearer',
            access_token: accessToken,
            expires_in: accessTokenDuration.as('seconds'),
            refresh_token: refreshToken,
        };
    }
}
