import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// import { config as awsConfig } from 'aws-sdk';
import config from './config';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        logger: ['error', 'warn'],
    });

    const port = process.env.PORT || 6000;

    await app.listen(port, () => {
        // awsConfig.update({
        //   region: config.AWS_REGION,
        //   accessKeyId: config.AWS_ACCESS_KEY_ID,
        //   secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
        // })

        console.log(`Server is running`);
        console.table({
            Port: port,
            Environment: config.NODE_ENV,
            'DB HOST': config.host,
        });
    });
}
bootstrap();
