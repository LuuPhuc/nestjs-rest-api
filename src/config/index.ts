import dotenv from 'dotenv';

const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
const envPath = `${__dirname}/../../.env.${NODE_ENV}`;

dotenv.config({ path: envPath });

// bcrypt
const SALT: number = +process.env.SALT || 10;

const config = {
    NODE_ENV,
    SALT,
    // Tokens
    JWT_SECRET: process.env.JWT_SECRET,
    ACCESS_TOKEN_EXP: process.env.ACCESS_TOKEN_EXP,
    REFRESH_TOKEN_EXP: process.env.REFRESH_TOKEN_EXP,
    // Database
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,

    // ID Recognition
    ID_RECOGNITION_URL: process.env.ID_RECOGNITION_URL,
    ID_RECOGNITION_API_KEY: process.env.ID_RECOGNITION_API_KEY,
    // AWS_REGION: process.env.AWS_REGION,
    // AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
    // AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
    // AWS_PUBLIC_BUCKET_NAME: process.env.AWS_PUBLIC_BUCKET_NAME,
};

export default config;
