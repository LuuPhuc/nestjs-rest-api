import config from '../../config';

export const IDRecognitionFPT = {
    URL: config.ID_RECOGNITION_URL,
    API_KEY: config.ID_RECOGNITION_API_KEY,
};

export const Methods = {
    GET: 'GET',
    POST: 'POST',
};
