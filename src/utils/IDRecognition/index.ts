import FormData from 'form-data';
import Fetch from 'node-fetch';
import { IDRecognitionFPT, Methods } from '../constants';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const IDRecognition = async (body): Promise<any> => {
    const data = new FormData();

    data.append('image_base64', body);

    await Fetch(IDRecognitionFPT.URL, {
        method: Methods.POST,
        headers: {
            api_key: IDRecognitionFPT.API_KEY,
        },
        body: data,
    })
        .then((response) => response.json())
        .then((resData) => {
            try {
                if (resData.data[0]) {
                    return { message: 'OK', data: resData.data[0] };
                } else {
                    return { message: 'Not OK' };
                }
            } catch (e) {
                return { message: e };
            }
        });
};
