// Nest
import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { mysqlOrmConfig } from './datasource/mysql.config';

// Modules
import { UserModule } from './modules/users/user.module';

@Module({
    imports: [
        TypeOrmModule.forRoot(mysqlOrmConfig),
        MulterModule.register({ dest: './upload' }),
        UserModule,
    ],
    controllers: [AppController],
})
export class AppModule {}
