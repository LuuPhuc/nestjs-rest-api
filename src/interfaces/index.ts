export enum UserType {
    Admin = 'admin',
    User = 'user',
}

export enum UserStatus {
    Active = 'active',
    Disable = 'disable',
}
