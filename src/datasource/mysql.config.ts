import { TypeOrmModuleOptions } from '@nestjs/typeorm';

import config from '../config';

const { host, username, password, database, port } = config;

export const mysqlOrmConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host,
    port,
    username,
    password,
    database,
    entities: ['dist/modules/**/*.entity.js'],
    synchronize: true,
};
