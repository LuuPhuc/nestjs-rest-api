import { Controller, Post, Get, Param, UseInterceptors, Body } from '@nestjs/common';

import { SerializerInterceptor } from '../../serialization/serializer.interceptor';
import { UploadedFiles } from '@nestjs/common/decorators';
import {
    FileFieldsInterceptor,
    FilesInterceptor,
} from '@nestjs/platform-express/multer/interceptors';

export class MulterService {}
