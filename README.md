# SSO API

## Setup
Build project:
```
- yarn build
- yarn build:watch
```

Start project:
```
- yarn start
- yarn start:watch (using nodemon)
```

Run console:
```
- yarn console
```

## Environment
Using `.env.local` for local environment, not push on github to avoid conflict
Using `.env.development` for development environment
Using `.env.staging` for staging environment
Using `.env.production` for production environment